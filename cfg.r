################################################################################
## Load Packages                                                              ##
################################################################################
library("gitProjectExtras")
sourceAllLibs("../extraLibs/")
install_and_source_library("raster")

sourceAllLibs()
sourceAllLibs("../rasterExtraFuns/")
sourceAllLibs("../rasterPlotFunctions/")
sourceAllLibs("libs/lpxcheckoutlibs/")
checkoutlpxOutConverter()
checkoutlpxBenchmarking()
sourceAllLibs("libs/lpxoutputconverter/")
sourceAllLibs("libs/lpxbenchmarking/")
quickPlot <- TRUE
################################################################################
## Setup project                                                              ##
################################################################################
setupProjectStructure()
graphics.off()

InputPaths <- cbind(
    Detrended = c(climFilePath    = '/home/doug/Documents2/climInputs/climData/detr/'    ,
                  maskFilePath    = '/home/doug/Documents2/climInputs/climData/'         ,
                  LgtnFilePath    = '/home/doug/Documents2/climInputs/climData/historic/',
                  soilFilePath    = '/home/doug/Documents2/climInputs/climData/',
                  humnFilePath    = '/home/doug/Documents2/climInputs/climData/detr/'    ,
                  agriFilePath    = '/home/doug/Documents2/climInputs/climData/detr/'    ,
                  CrbnFilePath    = '/home/doug/Documents2/climInputs/climData/detr/'    ),

    Historic  = c(climFilePath    = '/home/doug/Documents2/climInputs/climData/historic/',
                  maskFilePath    = '/home/doug/Documents2/climInputs/climData/'         ,
                  LgtnFilePath    = '/home/doug/Documents2/climInputs/climData/historic/',
                  soilFilePath    = '/home/doug/Documents2/climInputs/climData/'         ,
                  humnFilePath    = '/home/doug/Documents2/climInputs/climData/historic/',
                  agriFilePath    = '/home/doug/Documents2/climInputs/climData/historic/',
                  CrbnFilePath    = '/home/doug/Documents2/climInputs/climData/historic/'))

InputFiles <- cbind(
    Detrended = c(tempFile        = 'tas_19512000detr.nc'             ,
                  tempMinFile     = 'tasmin_19512000detr.nc'          ,
                  tempMaxFile     = 'tasmax_19512000detr.nc'          ,
                  precFile        = 'pr_19512000detr.nc'              ,
                  wetdaysFile     = 'wetdays_19512000detr.nc'         ,
                  windspeedFile   = 'uvas_19512000detr_new.nc'        ,
                  sunFile         = 'clt_19512000detr.nc'             ,
                  LgtnFile        = 'lightn_climatology_otd_mlnha.nc' ,
                  maskFile        = 'landmask_0k.nc'                  ,
                  soilFile        = 'soildata_0k.nc'                  ,
                  popFile         = 'popdens_fill_1850.nc'            ,
                  aNdFile         = '../historic/a_nd_fill.nc'        ,
                  cropFile        = 'cropland_1850.nc'                ,
                  pasFile         = 'pas_fill_1850.nc'                ,
                  CO2File         = 'co2__280.nc'                     ,
                  C13File         = 'c13_1850.nc'                     ,
                  C14NorthFile    = 'c14_1850.nc'                     ,
                  C14EquatorFile  = 'c14_1850.nc'                     ,
                  C14SouthFile    = 'c14_1850.nc'                     ),

     Historic = c(tempFile        = 'tas_18512006.nc'                 ,
                  tempMinFile     = 'tasmin_18512006.nc'              ,
                  tempMaxFile     = 'tasmax_18512006.nc'              ,
                  precFile        = 'pr_18512006.nc'                  ,
                  wetdaysFile     = 'wetdays_18512006.nc'             ,
                  windspeedFile   = 'uvas_18512006.nc'                ,
                  sunFile         = 'clt_18512006.nc'                 ,
                  LgtnFile        = 'lightn_climatology_otd_mlnha.nc' ,
                  maskFile        = 'landmask_0k.nc'                  ,
                  soilFile        = 'soildata_0k.nc'                  ,
                  popFile         = 'popdens_fill2.nc'                ,
                  aNdFile         = 'a_nd_fill.nc'                    ,
                  cropFile        = 'cropland_18512006.nc'            ,
                  pasFile         = 'pasture_18512006.nc'             ,
                  CO2File         = 'co2_18512006.nc'                 ,
                  C13File         = 'c13_18512006.nc'                 ,
                  C14NorthFile    = 'c14nth_18512006.nc'              ,
                  C14EquatorFile  = 'c14equ_18512006.nc'              ,
                  C14SouthFile    = 'c14sth_18512006.nc'              ))
