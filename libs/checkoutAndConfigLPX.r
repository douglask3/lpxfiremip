 checkoutAndConfigLPX <- function(versionNumber, runName, spinName, outName,
                                  outputVars = 'fpc_grid') {
    checkoutLPX(versionNumber)

    out := lpj.cfgStandard(InputPaths[, runName], InputFiles[, runName],
                           nyrsSpinup, nyrsRampup = 0, nyrsRun,
                           startYearsOutput, frequancyRunup = 1,
                           spinupFilePath, spinupFile = spinName, '',
                           outputFilePath, outputFile = outName , outputVars,
                           includeProjectVersionNumber = FALSE)

    return(out)
}
