lpj.cfgStandard <- function(paths, files, nyrsSpinup, nyrsRampup, nyrsRun,
                            startYearsOutput, frequancyRunup,
                            spinFilePath, spinupFile, rampupFile,
                            outputFilePath, outputFile, outputVars, ...) {
    lpj.cfg(paths['climFilePath'],
                files['tempFile'], files['tempMinFile'],
                files['tempMaxFile'], files['precFile'],
                files['wetdaysFile'], files['windspeedFile'],
                files['sunFile'], sunIsCloud = TRUE,
            paths['LgtnFilePath'], files['LgtnFile'],
            paths['maskFilePath'], files['maskFile'], maskVar = "mask",
                maskType = "BOOLEAN",
            paths['soilFilePath'], files['soilFile'], soilVar = "soiltype",
            paths['humnFilePath'], files['popFile'] , files['aNdFile'],
            paths['humnFilePath'], files['cropFile'], files['pasFile'],
            paths['CrbnFilePath'],
                files['CO2File'], CO2Var = 'co2',
                files['C13File'], C13Var = 'c13',
                files['C14NorthFile'], files['C14EquatorFile'],
                    files['C14SouthFile'], C14Var = 'c14',
            nyrsSpinup, nyrsRampup, nyrsRun, startYearsOutput, frequancyRunup,
            spinFilePath, spinupFile, rampupFile,
            outputFilePath, outputFile, outputVars, ...)
}
