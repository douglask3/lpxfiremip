performSpinUps <- function(versions, name = names(versions), outName = name) {
    p <<- 0; l <<- length(versions)
    mapply(performRun, versions, name, outName, MoreArgs = list(step1a = TRUE))
}


performRun <- function(versionNumber, spinName, outName = spinName,
                       filesID = 'Detrended', outID = 1, outputVars = "", ...) {

   OUT_FILES = checkoutAndConfigLPX(versionNumber, filesID, spinName,
                                     outName, outputVars)[[outID]]

    p <<- p + 1
    wait = p == l

    if (!all(file.exists(OUT_FILES))) {
        runLPX(wait = wait, ...)

        waitInverval(!wait)
    }
}

performHistoricRuns <- function(versions, outputVars, spinName = names(versions),
                                 namess = names(outputVars)) {
    p = 0; l = length(versions) * length(outputVars)
    performHistoricRun <- function(vars, name) {
        outsName = paste(spinName, name, sep = '-')

        mapply(performRun, versions, spinName, outsName,
               MoreArgs = list(filesID = 'Historic', outID = 3, step2  = TRUE,
                               outputVars = vars))

        convertFiles <- function(pattern)
            lpxFileConverter.standard(outputFilePath, pattern, vars,
                                     outDir = outputs_dir, comments = comments)
        dat = lapply(outsName, convertFiles)
    }

    mapply(performHistoricRun, outputVars, namess)
}
