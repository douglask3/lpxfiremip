plotDiff <- function(dat1, dat2, expNames,...) {
    dat1 = dat1[[1]]; dat2 = dat2[[1]]
    itemNames = names(dat1)

    runLPXbenchmarkingComparisons(comparisonInfo = list(dat1, dat2), asPackage = TRUE)
    browser()
    plotDiff.setup(length(dat1), expNames, itemNames)

    mapply(plotDiff.Item, dat1, dat2, itemNames,
           MoreArgs = list(expNames), ...)

    dev.off.gitWatermark()
    browser()
}

plotDiff.setup <- function(nplots, expNames, itemNames) {
    figName = paste(figs_dir, expNames, '-', itemNames, '.pdf', sep = "")

    pdf(figName, width = 9, height = nplots*1.5)
    par(mfrow = c(nplots, 3), mar = rep(0, 4), oma = c(0, 3, 0, 0))
}

plotDiff.Item <- function(dat1, dat2, itemName, expNames, index = 1) {
    limits = list(c(10, 20, 40, 60, 80), c(-50, -30, -10, 10, 30, 50))
    cols   = list(c('white', '#9900DD', '#110022'), c('brown', 'white', 'green'))

    dat1 = mean(dat1[[index]])
    dat2 = mean(dat2[[index]])

    plotDiff.plot(dat1,        limits[[1]], cols[[1]], expNames[1])
    mtext(side =2, itemName)
    plotDiff.plot(dat2,        limits[[1]], cols[[1]], expNames[2])
    plotDiff.plot(dat2 - dat1, limits[[2]], cols[[2]], 'diff'     )
}

plotDiff.plot <- function(dat, limits, cols, expNames) {

    plot_raster_from_raster(dat, limits = limits, cols = cols, quick = quickPlot)
    mtext(expNames)
}
