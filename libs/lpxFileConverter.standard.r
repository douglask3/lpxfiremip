lpxFileConverter.standard <- function(...)
    lpxFileConverter.lpx2011(PFTforms = list(TotalCover = 1:9,
                                             TreeCover  = 1:7,
                                             GrassCover = 8:9,
                                             EGcover    =c(1, 3, 4, 6),
                                             DecCover   =c(2, 5, 7),
                                             BLcover    =c(1:2,4:5,7),
                                             NLcover    =c(3,6)), ...)
