rm(list=ls())
source('cfg.r')
abcd <- function() source("testDiagnostics.r")


nyrsSpinup       = 1000
nyrsRun          = 155
startYearsOutput = 155
spinupFilePath   = 'temp/'


outputFilePath   = 'temp/'
outputVars       = list(vegFire  = c('fpc_grid', 'mfire_frac'),
                        fireDiag = c('mfuel_1hr_total', 'mlivegrass',
                                    'acflux_fire_grid', 'dlm', 'dlightn'))


versions         = c(control = "d4e9dbf")

comments         = "yayandwow"


performSpinUps(versions)
performHistoricRuns(versions, outputVars)
