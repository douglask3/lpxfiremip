rm(list=ls())
source('cfg.r')
abcd <- function() source("albiniComparison.r")


nyrsSpinup       = 2000
nyrsRun          = 155
startYearsOutput = 1
spinupFilePath   = 'temp/'


outputFilePath   = 'temp/'
outputVars       = list(vegFire = c('fpc_grid', 'mfire_frac'),
                        flux    = c('anpp_grid', 'arh_grid',
                                    'acflux_fire_grid'),
                        fAPAR   = c('mpar', 'mapar'),
                        carbon  = c('litter_ag', 'litter_bg',
                                    'cpool_fast', 'cpool_slow'),
                        height  = c('height', 'fpc_grid'))


versions         = c(control = "900a31b",
                     albini  = "2c3d844")

comments         = "yayandwow"


performSpinUps(versions)
performHistoricRuns(versions, outputVars)
runLPXbenchmarkingComparisons(lpx_dir  = outputs_dir, asPackage = TRUE)
